 <?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMyPackageNavbarTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create(config('mypackage.database.table'), function (Blueprint $table){
            $table->increments('id');
            $table->string('np_title')->nullable();
            $table->string('en_title')->nullable();
            $table->string('en_metadesc')->nullable();
            $table->integer('parent_id')->unsigned()->nullable();
            $table->text('np_description')->nullable();
            $table->text('en_description')->nullable();

            $table->integer('system')->nullable()->default(true);
            $table->string('scokeyword')->nullable();
            $table->integer('top')->nullable()->default(false);
            $table->string('link_url')->nullable();
            $table->string('url_target')->nullable();
            $table->string('column')->nullable();
            $table->integer('order')->nullable()->default(true);
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::drop(config('mypackage.database.table'));
    }
}


 