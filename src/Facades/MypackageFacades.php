<?php
namespace Santosh\Mypackage\Facades;

use Illuminate\Support\Facades\Facade;

class MypackageFacades extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'mypackage'; 
    }
    
}