<?php
namespace Santosh\Mypackage;
use Illuminate\Support\ServiceProvider;
class MypackageServiceProvider extends ServiceProvider
{
    /**
    * Publishes configuration file.
    *
    * @return  void
    */
    public function boot()
    {
        $this->publishes([
            __DIR__.'/config/Mypackage.php' => config_path('Mypackage.php'),
        ], 'mypackage');
    }
    /**
    * Make config publishment optional by merging the config from the package.
    *
    * @return  void
    */
    public function register()
    {
        $this->app->bind('mypackage', 'Santosh\\Mypackage\Mypackage');
        $this->mergeConfigFrom(
            __DIR__.'/config/Mypackage.php',
            'mypackage');

        // if ( ! class_exists('CreateMyPackageNavbarTable')) {
            // Publish the migration
            $timestamp = date('Y_m_d_His', time());

            $this->publishes([
                __DIR__.'/database/migrations/0000_00_00_000000_create_mypackagenavbars_table.php' => database_path('migrations/'.$timestamp.'_create_mypackagenavbars_table.php'),
            ], 'navbarmigrations');
        // }

    }
}