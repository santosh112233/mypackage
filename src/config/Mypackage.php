<?php
return [
    'log_git_data' => false,
    // You can specify the inputs from the user that should not be logged
    'ignore_input_fields' => ['password', 'confirm_password'],

    'database' => [
        'connection' => null,
        'table' => 'mypackagenavbars',
    ],
 
];